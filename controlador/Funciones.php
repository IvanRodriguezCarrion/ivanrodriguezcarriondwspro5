<?php
error_reporting(E_ALL);
ini_set('display_errors', '1');
include_once("Configuracion.php");
include_once "../modelo/ModeloFicheros.php";

function cabecera() {
    echo "<h1>" . Configuracion::$titulo . "</h1><hr/>\n";
}

function pie() {
    echo "<hr/><pre>" . Configuracion::$empresa . " " . Configuracion::$autor . " ";
    echo Configuracion::$curso . " " . Configuracion::$fecha . "</pre>\n";
}

function inicio() {
    echo "<align='right'><a href = '../index.php'>Inicio</a> </align>\n";
}

function recoge($valor) {
    if (isset($_REQUEST[$valor])) {
	$campo = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])));
    } else {
	$campo = "";
    };
    return $campo;
}

function comprobarArchivo($nombre) {
    if (!file_exists("../media/database/" . $nombre)) {
	$archivo = fopen("../media/database/" . $nombre, "w");
	fclose($archivo);
    }
}

function rellenarCBPersonas() {
    comprobarArchivo("personas.csv");
    $modeloFicheros = new ModeloFicheros();
    foreach ($modeloFicheros->readPersona() as $r):
	?>
	<option value ="<?php echo $r->__GET('id'); ?>"><?php echo $r->__GET('id'); ?></option>
	<?php
    endforeach;
}

function rellenarTablaPersona() {
    comprobarArchivo("personas.csv");
    $modeloFicheros = new ModeloFicheros();
    foreach ($modeloFicheros->readPersona() as $r):
	?>
	<tr>
	    <td><?php echo $r->__GET('id'); ?></td>
	    <td><?php echo $r->__GET('nombre'); ?></td>
	    <td><?php echo $r->__GET('apellido'); ?></td>
	    <td><?php echo $r->__GET('fechaNacimiento'); ?></td>
	</tr>
	<?php
    endforeach;
}

function rellenarTablaPerro() {
    comprobarArchivo("perros.csv");
    $modeloFicheros = new ModeloFicheros();
    foreach ($modeloFicheros->readPerro() as $r):
	?>
	<tr>
	    <td><?php echo $r->__GET('id'); ?></td>
	    <td><?php echo $r->__GET('nombre'); ?></td>
	    <td><?php echo $r->__GET('raza'); ?></td>
	    <td><?php echo $r->__GET('numChip'); ?></td>
	    <td><?php echo $r->__GET('propietario'); ?></td>
	</tr>
	<?php
    endforeach;
}

function calcularIDPersonas() {
    $modelo = new ModeloFicheros();
    $personas = $modelo->readPersona();
    $ultPersona = end($personas);
    $ultID = $ultPersona->__GET('id');
    $ultID++;
    echo $ultID;
}

function calcularIDPerros() {
    $modelo = new ModeloFicheros();
    $perros = $modelo->readPerro();
    $ultPerro = end($perros);
    $ultID = $ultPerro->__GET('id');
    $ultID++;
    echo $ultID;
}
?>