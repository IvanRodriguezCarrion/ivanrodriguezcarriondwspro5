<?php

include_once 'Persona.php';
include_once 'Perro.php';
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ModeloFicheros
 *
 * @author BlackBana
 */
class ModeloFicheros {

    private $fpersonas = "../media/database/personas.csv";
    private $fperros = "../media/database/perros.csv";

    public function createPersona($persona) {
	$f = fopen($this->fpersonas, "a");
	$linea = $persona->__GET('id') . ";"
		. $persona->__GET('nombre') . ";"
		. $persona->__GET('apellido') . ";"
		. $persona->__GET('fechaNacimiento') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function readPersona() {
	$personas = array();

	if ($archivo = fopen($this->fpersonas, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); // fgetcsv se comporta como fgets pero para ficheros csv
	    while ($token) {
		$persona = new Persona($token[0], $token[1], $token[2], $token[3]);
		array_push($personas, $persona);
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    echo "Esto es un error al abrir";
	}

	return $personas;
    }

    public function createPerro($perro) {
	$f = fopen($this->fperros, "a");
	$linea = $perro->__GET('id') . ";"
		. $perro->__GET('nombre') . ";"
		. $perro->__GET('raza') . ";"
		. $perro->__GET('numChip') . ";"
		. $perro->__GET('propietario') . "\r\n";
	fwrite($f, $linea);
	fclose($f);
    }

    public function readPerro() {
	$perros = array();

	if ($archivo = fopen($this->fperros, "r")) {
	    $token = fgetcsv($archivo, 0, ";"); // fgetcsv se comporta como fgets pero para ficheros csv
	    while ($token) {
		$perro = new Perro($token[0], $token[1], $token[2], $token[3], $token[4]);
		array_push($perros, $perro);
		//agregar funcion recuperar nombre duenyo
		$token = fgetcsv($archivo, 0, ";");
	    }
	    fclose($archivo);
	} else {
	    //errorres
	}

	return $perros;
    }

}
