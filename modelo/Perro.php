<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Perro
 *
 * @author BlackBana
 */
class Perro {
    private $id;
    private $nombre;
    private $raza;
    private $numChip;
    private $propietario;
    
    public function __CONSTRUCT($id, $nombre, $raza, $numChip, $propietario) {
	$this->id = $id;
	$this->nombre = $nombre;
	$this->raza = $raza;
	$this->numChip = $numChip;
	$this->propietario = $propietario;
    }
    
    public function __GET($k) {
	return $this->$k;
    }

    public function __SET($k, $v) {
	return $this->$k = $v;
    }
}
