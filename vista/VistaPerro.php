<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Perros</title>
	<meta charset="UTF-8">
	<?php
	include_once "../controlador/Funciones.php";
	?>
    </head>
    <body >
	<?php
	cabecera();
	?>
	<h1> Alta de nuevos perros</h1>
        <div>
            <div>

                <form action="../controlador/ControladorPerro.php" method="post" class="pure-form pure-form-stacked" >
                    <table >
			<tr>
                            <th >Identificador</th>
                            <td><input type="text" name="id" value="<?php calcularIDPerros() ?>" readonly="readonly" /></td>
                        </tr>
			<tr>
                            <th >Nombre</th>
                            <td><input type="text" name="nombre" value=""  /></td>
                        </tr>
                        <tr>
                            <th >Raza</th>
                            <td><input type="text" name="raza" value=""  /></td>
                        </tr>
			<tr>
                            <th >Número Chip</th>
                            <td><input type="text" name="numChip" value=""  /></td>
                        </tr>
                        <tr>
                            <th >Proprietario</th>
                            <td>
                                <select name="propietario" >
				    <?php
				    rellenarCBPersonas();
				    ?>
                                </select>
                            </td>
                        </tr>
			<tr>
                            <td colspan="2">
                                <button type="submit">Guardar</button>
                            </td>
                        </tr>
                    </table>
                </form>

                <table>
                    <thead>
                        <tr>
			    <th>ID</th>
                            <th >Nombre</th>
                            <th >Raza</th>
                            <th >Número de chip</th>
                            <th >Propietario</th>
                        </tr>
                    </thead>

		    <!--Aqui podemos encapsular esta función en un archivo distinto y meterla como si fuese un mixin de sass-->
		    <?php
		    rellenarTablaPerro();
		    ?>
                </table>     

            </div>
        </div>
	<?php
	pie();
	inicio();
	?>
    </body>
</html>