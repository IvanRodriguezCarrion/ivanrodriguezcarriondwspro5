<!DOCTYPE html>
<html lang="es">
    <head>
        <title>Gestión de Personas</title>
	<meta charset="UTF-8">
	<?php
	include_once "../controlador/Funciones.php";
	?>
    </head>
    <body >
	<?php
	cabecera();
	?>
	<h1> Alta de nuevas personas</h1>
        <div>
            <div>

                <form action="../controlador/ControladorPersona.php" method="post" class="pure-form pure-form-stacked" >
                    <table >
			<tr>
                            <th >Identificador</th>
                            <td><input type="text" name="id" value="<?php calcularIDPersonas() ?>" readonly="readonly" /></td>
                        </tr>
			<tr>
                            <th >Nombre</th>
                            <td><input type="text" name="nombre" value=""  /></td>
                        </tr>
                        <tr>
                            <th >Apellido</th>
                            <td><input type="text" name="apellido" value=""  /></td>
                        </tr>
                        <tr>
                            <th >Fecha</th>
                            <td><input type="text" name="fechaNacimiento" value=""  /></td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <button type="submit">Guardar</button>
                            </td>
                        </tr>
                    </table>
                </form>

                <table>
                    <thead>
                        <tr>
			    <th>ID</th>
                            <th >Nombre</th>
                            <th >Apellido</th>
                            <th >Nacimiento</th>
                        </tr>
                    </thead>

		    <!--Aqui podemos encapsular esta función en un archivo distinto y meterla como si fuese un mixin de sass-->
		    <?php
		    rellenarTablaPersona();
		    ?>
                </table>     

            </div>
        </div>
	<?php
	pie();
	inicio();
	?>
    </body>
</html>